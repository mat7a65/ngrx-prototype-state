import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { GetstartedComponent } from './components/getstarted/getstarted.component';
import { DocsComponent } from './components/docs/docs.component';
import { BlogComponent } from './components/blog/blog.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {sidenavReducer} from './reducers/sidenav.reducer';
import { MyprofileComponent } from './components/myprofile/myprofile.component';
import { ShoppingcardComponent } from './components/shoppingcard/shoppingcard.component';
import { InfoComponent } from './components/info/info.component';

@NgModule({
  declarations: [
    AppComponent,
    GetstartedComponent,
    DocsComponent,
    BlogComponent,
    MyprofileComponent,
    ShoppingcardComponent,
    InfoComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    StoreModule.forRoot({showSidenav: sidenavReducer}, {}),
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatSidenavModule,
    MatListModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
