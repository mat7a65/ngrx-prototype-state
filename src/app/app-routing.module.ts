import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GetstartedComponent} from './components/getstarted/getstarted.component';
import {DocsComponent} from './components/docs/docs.component';
import {BlogComponent} from './components/blog/blog.component';
import {MyprofileComponent} from './components/myprofile/myprofile.component';
import {ShoppingcardComponent} from './components/shoppingcard/shoppingcard.component';
import {InfoComponent} from './components/info/info.component';

const routes: Routes = [
  {path: 'gettingstarted', component: GetstartedComponent},
  {path: 'docs', component: DocsComponent},
  {path: 'blog', component: BlogComponent},
  {path: 'myprofile', component: MyprofileComponent},
  {path: 'shoppingcard', component: ShoppingcardComponent},
  {path: 'info', component: InfoComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
