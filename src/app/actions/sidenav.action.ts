import { createAction } from '@ngrx/store';

export const changeStateSidebar = createAction('[Sidenav] ChangeState');
