import {createReducer, on} from '@ngrx/store';
import {changeStateSidebar} from '../actions/sidenav.action';

export const initialState = false;

// tslint:disable-next-line:variable-name
const _sidenavReducer = createReducer(
  initialState,
  on(changeStateSidebar, (state) => !state),
);

export function sidenavReducer(state, action): boolean {

  console.log('State before action: ' + state);
  console.log('Perform action: ');
  console.log(action);

  return _sidenavReducer(state, action);
}
