import {Component} from '@angular/core';
import {Observable} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {changeStateSidebar} from './actions/sidenav.action';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ngrx-prototype-state';

  showSidenav$: Observable<boolean>;

  constructor(private store: Store<{ showSidenav: boolean }>) {
    this.showSidenav$ = store.pipe(select('showSidenav'));
  }

  changeStateSidebar(): void {
    this.store.dispatch(changeStateSidebar());
  }
}
